import model1 as md1
import model2 as md2
import model3 as md3
import model4 as md4


if __name__ == '__main__':

    X_train, X_test, Y_train, Y_test = md2.load_data("pizza_request_dataset.json")
    X_train134 = [r[0] for r in X_train]
    X_test134 = [r[0] for r in X_test]
    with open("classification_ROAP.csv", 'w') as report_file:
        print "first model"
        X_train1, X_test1 = md1.extract_n_grams(X_train134, X_test134, False)
        report_file.write("model,sample,accuracy,precision,recall,f1 score,specificity,roc auc score\n")
        md2.classify_data(X_train1, X_test1, Y_train, Y_test, report_file, 1)

        print "second model"
        X_train2 = [r[1:] for r in X_train]
        X_test2 = [r[1:] for r in X_test]
        md2.classify_data(X_train2, X_test2, Y_train, Y_test, report_file, 2)

        print "third model"
        mod3 = md3.Model3()
        X_train3 = mod3.extract_regexp(X_train134, Y_train)
        X_test3 = mod3.extract_regexp(X_test134, Y_test)
        md3.classify_data(X_train3, X_test3, Y_train, Y_test, report_file, 3)

        print "forth model"
        mod4 = md4.Model4()
        X_train4, X_test4 = mod4.build_data(X_train134, X_test134)
        md3.classify_data(X_train4, X_test4, Y_train, Y_test, report_file, 4)
    report_file.close()