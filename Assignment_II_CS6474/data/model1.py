import data.read_dataset as rd
import sklearn.feature_extraction.text as fet
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import LinearSVC
from sklearn.linear_model import SGDClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.linear_model import RidgeClassifier

import model2 as md2
import numpy as np
from random import seed


def load_data(input_path1, train_size=0.9):
    dataset = rd.read_dataset(input_path1)
    # x_data = [r['request_text'] for r in dataset]
    attributes = [
        # text - 0
        'request_text', # longstring
        # activity - 1,15
        # "post_was_edited", # bool
        "requester_account_age_in_days_at_request", # float
        "requester_account_age_in_days_at_retrieval", # float
        "requester_days_since_first_post_on_raop_at_request", # float
        "requester_days_since_first_post_on_raop_at_retrieval", # float
        "requester_number_of_comments_at_request", # int
        "requester_number_of_comments_at_retrieval", # int
        "requester_number_of_comments_in_raop_at_request", # int
        "requester_number_of_comments_in_raop_at_retrieval", # int
        "requester_number_of_posts_at_request", # int
        "requester_number_of_posts_at_retrieval", # int
        "requester_number_of_posts_on_raop_at_request", # int
        "requester_number_of_posts_on_raop_at_retrieval", # int
        "requester_number_of_subreddits_at_request", #int
        # "requester_subreddits_at_request", # sequence!
        # reputation - 16,22
        "number_of_downvotes_of_request_at_retrieval", # int
        "number_of_upvotes_of_request_at_retrieval", # int
        "requester_upvotes_minus_downvotes_at_request", # int
        "requester_upvotes_minus_downvotes_at_retrieval", #int
        "requester_upvotes_plus_downvotes_at_request", #int
        "requester_upvotes_plus_downvotes_at_retrieval", # int
        # "requester_user_flair" # string
    ]

    x_data = [[r[attr] for attr in attributes] for r in dataset]
    y_data = [r['requester_received_pizza'] for r in dataset]
    seed(5)
    X_train, X_test, Y_train, Y_test = train_test_split(x_data, y_data, train_size=train_size, random_state=5)
    return X_train, X_test, Y_train, Y_test


def extract_n_grams(X_train, X_test, save_ngrams=False):
    vectorizer = fet.CountVectorizer(ngram_range=(1, 2), analyzer="word", min_df=0, stop_words='english')
    X = vectorizer.fit_transform(X_train)
    X_sum = X.sum(axis=0)
    scores_array = np.array(X_sum)
    sorted_scores = (-scores_array).argsort().flatten()
    bigram_count = 0
    unigram_count = 0
    top_scores = np.zeros(shape=(1000,), dtype=int)
    ft_names = vectorizer.get_feature_names()
    l = 0
    while (unigram_count < 500) | (bigram_count < 500):
        ssindex = sorted_scores[l]
        if (len(ft_names[ssindex].split(" ")) == 1) & (unigram_count < 500):
            top_scores[unigram_count+bigram_count] = ssindex
            unigram_count += 1
        elif (len(ft_names[ssindex].split(" ")) == 2) & (bigram_count < 500):
            top_scores[unigram_count + bigram_count] = ssindex
            bigram_count += 1
        l += 1
    if save_ngrams:
        save_to_file(vectorizer, X_sum, "./best_bi-uni_grams.csv", top_scores)

    trainx = X[:, top_scores]
    testx = vectorizer.transform(X_test)[:, top_scores]
    return trainx, testx


def save_to_file(vect, term_doc_mat, output_path, sorted_scores):
    features = vect.get_feature_names()
    word_scores = term_doc_mat.A1
    with open(output_path, "w") as output_file:
        output_file.write("word, count\n")
        for l in sorted_scores:
            towrite = str(features[l]) + "," + str(word_scores[l]) + "\n"
            output_file.write(towrite)
    output_file.close()

# def classify_data(X_train, X_test, Y_train, Y_test, report_file, modelid):
#     t0 = time()
#     print("preprocessing time : {}".format(time() - t0))
#     classifier_name = 'LinearSVC'
#     bc = train_validate_phase(classifier_name, X_train, Y_train, report_file, modelid)
#     begin_time = time()
#     prediction = bc.predict(X_test)
#     end_time = time() - begin_time
#     print("best clf {0} testing time : {1}\n".format(bc, end_time))
#     # report_file.write("clf {0} testing time : {1}\n".format(bc, end_time))
#     accuracy = accuracy_score(Y_test, prediction)
#     print("mean accuracy : {}\n".format(accuracy))
#     # report_file.write("best clf {0} mean accuracy: {1}\n".format(bc, accuracy))
#     precision = precision_score(Y_test, prediction)
#     recall = recall_score(Y_test, prediction)
#     specificity = recall_score(Y_test, prediction, pos_label=0)
#     f1score = f1_score(Y_test, prediction)
#     aucscore = roc_auc_score(Y_test, prediction)
#     report_file.write(",".join(["model{}".format(modelid),"out of sample",str(accuracy), str(precision), str(recall), str(f1score), str(specificity), str(aucscore)]) + "\n")
#     # report = classification_report(Y_test, prediction, digits=4)
#     # report_file.write(report)


# def train_validate_phase(classif_name, X_train, Y_train, report_file, modelid, write_r=False):
#     best_accuracy = 0
#     best_f1_score = 0
#     best_classifier = None
#     kfold = KFold(n_splits=5)
#     kfold.get_n_splits(X_train, Y_train)
#     chunks = kfold.split(X_train, Y_train)
#     accuracyscore = 0
#     precision = 0
#     specificity = 0
#     recall = 0
#     f1score = 0
#     aucscore = 0
#     for train_index, test_index in chunks:
#         clf = classifier_factory(classif_name)
#         train_x_chunk = X_train[train_index, :]
#         test_x_chunk = X_train[test_index, :]
#         train_y_chunk = [Y_train[i] for i in train_index]
#         test_y_chunk = [Y_train[i] for i in test_index]
#         begin_time = time()
#         clf.fit(train_x_chunk, train_y_chunk)
#         end_time = time()-begin_time
#         if write_r : report_file.write("clf {0} learning time : {1} \n".format(classif_name, end_time))
#         print("clf {0} learning time : {1}".format(classif_name, end_time))
#         begin_time = time()
#         prediction = clf.predict(test_x_chunk)
#         end_time = time()-begin_time
#         print("clf {0} validation time : {1}\n".format(classif_name, end_time))
#         if write_r: report_file.write("clf {0} validation time : {1}\n".format(classif_name, end_time))
#         accuracy = accuracy_score(test_y_chunk, prediction)
#         print("mean accuracy : {}\n".format(accuracy))
#         if write_r : report_file.write("clf {0} mean accuracy: {1}\n".format(classif_name, accuracy))
#         # report = classification_report(test_y_chunk, prediction, digits=4)
#         # if write_r : report_file.write(report)
#         accuracyscore += accuracy
#         precision += precision_score(test_y_chunk, prediction)
#         recall += recall_score(test_y_chunk, prediction, pos_label=1)
#         specificity += recall_score(test_y_chunk, prediction, pos_label=0)
#         clf_f1_score = f1_score(test_y_chunk, prediction)
#         f1score += clf_f1_score
#         aucscore += roc_auc_score(test_y_chunk, prediction)
#
#         if best_f1_score < clf_f1_score:
#             best_classifier = clf
#             best_accuracy = accuracy
#         elif best_f1_score == f1_score(test_y_chunk, prediction):
#             if best_accuracy < accuracy:
#                 best_classifier = clf
#                 best_accuracy = accuracy
#     accuracyscore /= 5.0
#     precision /= 5.0
#     recall /= 5.0
#     f1score /= 5.0
#     aucscore /= 5.0
#     specificity /= 5.0
#     report_file.write(",".join(["model{}".format(modelid),"in sample",str(accuracyscore), str(precision), str(recall), str(f1score), str(specificity),str(aucscore)])+"\n")
#
#     return best_classifier


def classifier_factory(name):
    return {
        "MultinomialNB": MultinomialNB(),
        "LinearSVC": LinearSVC(loss='squared_hinge', penalty='l2', dual=False, tol=1e-3),
        "SGDClassifier": SGDClassifier(),
        "RidgeClassifier": RidgeClassifier(),
        "AdaBoostClassifier": AdaBoostClassifier(n_estimators=5),
        "RandomForestClassifier": RandomForestClassifier(n_estimators=4, criterion='gini', min_samples_split=2)
    }.get(name)


if __name__=="__main__":
    X_train, X_test, Y_train, Y_test = load_data("pizza_request_dataset.json")
    X_train1 = [r[0] for r in X_train]
    X_test1 = [r[0] for r in X_test]
    X_train1, X_test1 = extract_n_grams(X_train1, X_test1, True)
    with open("model_1_results.csv", 'w') as report_file:
        report_file.write("sample,accuracy,precision,recall,f1 score,specificity,roc auc score\n")
        md2.classify_data(X_train1, X_test1, Y_train, Y_test, report_file, 1)
    report_file.close()

