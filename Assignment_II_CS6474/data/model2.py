import data.read_dataset as rd
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, f1_score, accuracy_score, precision_score, recall_score, roc_auc_score, precision_recall_fscore_support
import model1 as md1

from time import time
from random import seed


def load_data(input_path1, train_size=0.9):
    dataset = rd.read_dataset(input_path1)
    # x_data = [r['request_text'] for r in dataset]
    attributes = [
        # text - 0
        'request_text', # longstring
        # activity - 1,15
        # "post_was_edited", # bool
        "requester_account_age_in_days_at_request", # float
        "requester_account_age_in_days_at_retrieval", # float
        "requester_days_since_first_post_on_raop_at_request", # float
        "requester_days_since_first_post_on_raop_at_retrieval", # float
        "requester_number_of_comments_at_request", # int
        "requester_number_of_comments_at_retrieval", # int
        "requester_number_of_comments_in_raop_at_request", # int
        "requester_number_of_comments_in_raop_at_retrieval", # int
        "requester_number_of_posts_at_request", # int
        "requester_number_of_posts_at_retrieval", # int
        "requester_number_of_posts_on_raop_at_request", # int
        "requester_number_of_posts_on_raop_at_retrieval", # int
        "requester_number_of_subreddits_at_request", #int
        # "requester_subreddits_at_request", # sequence!
        # reputation - 16,22
        "number_of_downvotes_of_request_at_retrieval", # int
        "number_of_upvotes_of_request_at_retrieval", # int
        "requester_upvotes_minus_downvotes_at_request", # int
        "requester_upvotes_minus_downvotes_at_retrieval", #int
        "requester_upvotes_plus_downvotes_at_request", #int
        "requester_upvotes_plus_downvotes_at_retrieval", # int
        # "requester_user_flair" # string
    ]
    # req_user_flair_labels = np.array([r['requester_user_flair'] for r in dataset])
    # labelenc = preprocessing.LabelEncoder()
    # labelenc.fit(req_user_flair_labels)
    x_data = [[r[attr] if r[attr] is not None else "" for attr in attributes] for r in dataset]
    # x_data = np.array([parse_data(r, attributes, labelenc) for r in dataset])
    y_data = [r['requester_received_pizza'] for r in dataset]
    seed(5)
    X_train, X_test, Y_train, Y_test = train_test_split(x_data, y_data, train_size=train_size, random_state=5)
    return X_train, X_test, Y_train, Y_test


def parse_data(row, attributes, labelenc):
    parsed_line = []
    for attr in attributes:
        if attr =="post_was_edited":
            if row["post_was_edited"] == "true":
                parsed_line.append(1.0)
            else:
                parsed_line.append(0.0)
        elif attr == "requester_user_flair":
            a = labelenc.transform([row['requester_user_flair']])
            parsed_line.append(float(a[0]))
        elif row[attr] is not None and attr != "request_text":
            parsed_line.append(float(row[attr]))
        elif row[attr] is not None and attr == "request_text":
            parsed_line.append(row[attr])
        else: parsed_line.append("")
    return parsed_line


def classify_data(trainx, testx, trainy, testy, report_file, modelid):
    classifier_name = 'LinearSVC'
    bc = train_validate_phase(classifier_name, trainx, trainy, report_file, modelid)
    begin_time = time()
    prediction = bc.predict(testx)
    print prediction
    end_time = time() - begin_time
    print("best clf {0} testing time : {1}\n".format(bc, end_time))
    accuracy = accuracy_score(testy, prediction)
    precisions, recalls, f1scores, supports = precision_recall_fscore_support(testy, prediction)
    aucscore = roc_auc_score(testy, prediction)
    print("mean accuracy : {}\n".format(accuracy))
    report_file.write(",".join(["model{}".format(modelid),"out of sample", str(accuracy), str(precisions[1]), str(recalls[1]), str(f1scores[1]),
                                str(recalls[0]),
                                str(aucscore)]) + "\n")
    # report = classification_report(Y_test, prediction, digits=4)
    # report_file.write(report)


def train_validate_phase(classif_name, trainx, trainy, report_file, modelid, write_r=False):
    clf = md1.classifier_factory(classif_name)
    begin_time = time()
    clf.fit(trainx, trainy)
    end_time = time()-begin_time
    if write_r: report_file.write("clf {0} learning time : {1} \n".format(classif_name, end_time))
    print("clf {0} learning time : {1}".format(classif_name, end_time))
    begin_time = time()
    prediction = clf.predict(trainx)
    end_time = time()-begin_time
    # report = classification_report(test_y_chunk, prediction, digits=4)
    # if write_r : report_file.write(report)
    accuracyscore = accuracy_score(trainy, prediction)
    precisions, recalls, f1scores, supports = precision_recall_fscore_support(trainy, prediction)
    aucscore = roc_auc_score(trainy, prediction)
    report_file.write(",".join(["model{}".format(modelid),"in sample", str(accuracyscore), str(precisions[1]), str(recalls[1]),
                                            str(f1scores[1]),
                                            str(recalls[0]), str(aucscore)])+"\n")
    print("clf {0} validation time : {1}\n".format(classif_name, end_time))
    if write_r: report_file.write("clf {0} validation time : {1}\n".format(classif_name, end_time))

    print("mean accuracy : {}\n".format(accuracyscore))
    if write_r: report_file.write("clf {0} mean accuracy: {1}\n".format(classif_name, accuracyscore))
    return clf


def save_to_file(vect, term_doc_mat, output_path, sorted_scores):
    features = vect.get_feature_names()
    word_scores = term_doc_mat.A1
    with open(output_path, "w") as output_file:
        output_file.write("word, count\n")
        for l in sorted_scores:
            towrite = str(features[l]) + "," + str(word_scores[l]) + "\n"
            output_file.write(towrite)
    output_file.close()


if __name__=="__main__":
    X_train, X_test, Y_train, Y_test = load_data("pizza_request_dataset.json")
    print "second model"
    X_train2 = [r[1:] for r in X_train]
    X_test2 = [r[1:] for r in X_test]
    with open("model_2_results.csv", 'w') as report_file:
        report_file.write("sample,accuracy,precision,recall,f1 score,specificity,roc auc score\n")
        classify_data(X_train2, X_test2, Y_train, Y_test, report_file, 2)
    report_file.close()
