from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import classification_report, f1_score, accuracy_score, precision_score, recall_score, roc_auc_score, precision_recall_fscore_support
from sklearn.svm import LinearSVC
from sklearn.linear_model import SGDClassifier, RidgeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier

import numpy as np
from time import time
import model2 as md2
import re


class Model3:
    def __init__(self):
        narr_dir = '../resources/narratives/'
        self.narrative_paths = {narr_dir + 'desire.txt': 0, narr_dir + 'family.txt': 1, narr_dir + "job.txt": 2,
                                narr_dir + 'money.txt': 3, narr_dir + 'student.txt': 4}

    def extract_regexp(self, data, labels):
        scores = np.zeros(shape=(len(labels), len(self.narrative_paths)))
        l = 0
        for r in data:
            text_size = float(len(r.split(" ")))
            for input_path in self.narrative_paths:
                narrative_words = load_words(input_path)
                words = [nw for nw in narrative_words]
                reg_words = "|".join(words)
                tokenization_pattern = r'''(?:^|(?<= ))(''' + reg_words + ''')(?:(?= )|$)'''
                result = re.findall(tokenization_pattern, r, re.U)
                scores[l, self.narrative_paths[input_path]] = float(len(result))/text_size
            l += 1
        return scores


def classify_data(trainx, testx, trainy, testy, report_file, modelid):
    classifier_name = 'LinearSVC'
    bc = train_validate_phase(classifier_name, trainx, trainy, report_file, modelid)
    begin_time = time()
    prediction = bc.predict(testx)
    end_time = time() - begin_time
    print("best clf {0} testing time : {1}\n".format(bc, end_time))
    accuracy = accuracy_score(testy, prediction)
    precisions, recalls, f1scores, supports = precision_recall_fscore_support(testy, prediction)
    aucscore = roc_auc_score(testy, prediction)
    # report_file.write("best clf {0} mean accuracy: {1}\n".format(bc, accuracy))
    # report_file.write("clf {0} testing time : {1}\n".format(bc, end_time))
    print("mean accuracy : {}\n".format(accuracy))

    report_file.write(",".join(["model{}".format(modelid),"out of sample", str(accuracy), str(precisions[1]), str(recalls[1]), str(f1scores[1]),
                                str(recalls[0]),
                                str(aucscore)]) + "\n")

    print("auc score : {}".format(aucscore))
    # report = classification_report(Y_test, prediction, digits=4)
    # report_file.write(report)


def train_validate_phase(classif_name, trainx, trainy, report_file, modelid, write_r=False):
    clf = classifier_factory(classif_name)
    begin_time = time()
    clf.fit(trainx, trainy)
    end_time = time()-begin_time
    if write_r: report_file.write("clf {0} learning time : {1} \n".format(classif_name, end_time))
    print("clf {0} learning time : {1}".format(classif_name, end_time))
    begin_time = time()
    prediction = clf.predict(trainx)
    end_time = time()-begin_time

    # report = classification_report(test_y_chunk, prediction, digits=4)
    # if write_r : report_file.write(report)
    accuracyscore = accuracy_score(trainy, prediction)
    precisions, recalls, f1scores, supports = precision_recall_fscore_support(trainy, prediction)
    aucscore = roc_auc_score(trainy, prediction)
    report_file.write(",".join(["model{}".format(modelid), "in sample", str(accuracyscore), str(precisions[1]), str(recalls[1]),
                                            str(f1scores[1]),
                                            str(recalls[0]), str(aucscore)])+"\n")
    print("clf {0} validation time : {1}\n".format(classif_name, end_time))
    # if write_r: report_file.write("clf {0} validation time : {1}\n".format(classif_name, end_time))

    print("mean accuracy : {}\n".format(accuracyscore))
    # if write_r: report_file.write("clf {0} mean accuracy: {1}\n".format(classif_name, accuracyscore))

    print("auc score : {}".format(aucscore))
    return clf


def load_words(input_path):
    words = []
    with open(input_path, mode='r') as narr_file:
        for r in narr_file:
            words.append(r.rstrip("\n"))
    narr_file.close()
    return words


def classifier_factory(name):
    return {
        "MultinomialNB": MultinomialNB(),
        "LinearSVC": LinearSVC(loss='squared_hinge', penalty='l2', dual=False, class_weight={0: 0.3, 1: .91}),
        "SGDClassifier": SGDClassifier(),
        "RidgeClassifier": RidgeClassifier(),
        "AdaBoostClassifier": AdaBoostClassifier(n_estimators=5),
        "RandomForestClassifier": RandomForestClassifier(n_estimators=4, criterion='gini', min_samples_split=2)
    }.get(name)


if __name__=="__main__":
    X_train, X_test, Y_train, Y_test = md2.load_data("pizza_request_dataset.json")
    X_train3 = [r[0] for r in X_train]
    X_test3 = [r[0] for r in X_test]
    print "begin third phase"
    md3 = Model3()
    X_train3 = md3.extract_regexp(X_train3, Y_train)
    X_test3 = md3.extract_regexp(X_test3, Y_test)
    with open('model_3_results.csv', 'w') as report_file:
        report_file.write("model,sample,accuracy,precision,recall,f1 score,specificity,roc auc score\n")
        classify_data(X_train3, X_test3, Y_train, Y_test, report_file, 3)
    report_file.close()
