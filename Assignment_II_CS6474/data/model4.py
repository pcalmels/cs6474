import numpy as np
import data.model2 as md2
import data.model3 as md3
import re


class Model4:
    def __init__(self):
        self.dict_path = '../resources/MoralFoundations.dic'

    def build_data(self, trainx, testx):
        feature_dict = {}
        moral_dict = {}
        with open(self.dict_path, 'r') as moralfound:
            moralfound.readline()
            in_header = True
            for r in moralfound.readlines():
                # catch the end of the header
                if "%" in r and in_header:
                    in_header = False
                # grab the header
                elif in_header:
                    result = r.split()
                    feature_dict[int(result[0])] = result[1]
                    moral_dict[int(result[0])] = []
                else:
                    result = r.split()
                    if result:
                        for val in result[1:]:
                            moral_dict[int(val)].append(result[0])
        moralfound.close()
        train_feats = np.zeros(shape=(len(trainx), len(feature_dict.keys())+1))
        test_feats = np.zeros(shape=(len(testx), len(feature_dict.keys())+1))
        for text_id in range(len(trainx)):
            text = trainx[text_id]
            sample_size = float(len(text.split(' ')))
            for foundation_index in moral_dict:
                regex_keywords = '|'.join([k if k[-1] != "*" else k[:-1]+'[a-z]*.*' for k in moral_dict[foundation_index]])
                regex_tokenizer = r'''(?:^|(?<= ))(''' + regex_keywords + ''')(?:(?= )|$)'''
                result = re.findall(regex_tokenizer, text, re.U)
                train_feats[text_id, foundation_index] += len(result)/sample_size

        for text_id in range(len(testx)):
            text = testx[text_id]
            sample_size = float(len(text.split(' ')))
            for foundation_index in moral_dict:
                regex_keywords = '|'.join(
                    [k if k[-1] != "*" else k[:-1] + '[a-z]*.*' for k in moral_dict[foundation_index]])
                regex_tokenizer = r'''(?:^|(?<= ))(''' + regex_keywords + ''')(?:(?= )|$)'''
                result = re.findall(regex_tokenizer, text, re.U)
                test_feats[text_id, foundation_index] += len(result) / sample_size
        trainx = train_feats[:, 1:]
        testx = test_feats[:, 1:]
        return trainx, testx


if __name__=="__main__":
    X_train, X_test, Y_train, Y_test = md2.load_data("pizza_request_dataset.json")
    X_train4 = [r[0] for r in X_train]
    X_test4 = [r[0] for r in X_test]
    print "forth model"
    md4 = Model4()
    X_train4, X_test4 = md4.build_data(X_train4, X_test4)
    with open('model_4_results.csv', 'w') as report_file:
        report_file.write("model,sample,accuracy,precision,recall,f1 score,specificity,roc auc score\n")
        md3.classify_data(X_train4, X_test4, Y_train, Y_test, report_file, 4)
    report_file.close()
