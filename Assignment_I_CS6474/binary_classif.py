import scipy
import sklearn.feature_extraction.text as fet
import random
from collections import Counter


def extract_grams(file_directory="data/pos_examples_PosSentiment.txt", n=3):

    fet.CountVectorizer(file_directory, lowercase=True, ngram_range=(1,3), analyzer="word")

    with open(file_directory, encoding='utf-8') as data_file:
        # tweets_list = []
        n_gram_count = Counter()

        for line in data_file:
            # print(line)
            # tweets_list.append(line)
            splitted_line = [cleaned(x) for x in line.split() if not isRT(cleaned(x))]
            print(splitted_line)
            n_gram_count += Counter(zip(*[splitted_line[i:] for i in range(n)]))
        # data = [line for line in data_file]
    # print("Number of records: %d" % len(data))
    return n_gram_count


def isRT(word):
    return "@" in word or "RT" in word or "www." in word or "http://" in word or word == ""


def cleaned(word):
    return word.strip().replace("\\", "").replace(".", "").replace("!", "").replace("?", "").lower()


def find_best_grams():
    weighted_n_grams = Counter()
    for n in range(1, 3):
        weighted_n_grams += extract_grams(n=n)
    return weighted_n_grams.most_common(5000)


# FIRST QUESTION
def first_question(output_file_name="most_common_grams_output.csv"):

    best_grams = find_best_grams()
    with open(output_file_name, "w") as output_file:
        # HEADER
        output_file.write("gram, count\n")
        for k in best_grams:
            s = str(k)+","+str(best_grams[k])+"\n"
            output_file.write(s)
    output_file.close()


if __name__ == '__main__':
    grams = []
    # for n in range(1, 4):
    #     grams.append(extract_grams("data/pos_examples_PosSentiment.txt", n))
    grams.append(extract_grams("data/pos_examples_PosSentiment.txt"))
    print("----------------------------")
    print(grams)



    # x = Counter()
    # y = Counter({"b":3, "c":1})
    # x+=Counter(["a","b", "b"])
    # print(x+y)
    # s = "hello hello world RT RT @"
    # splitted_line = Counter([x.strip() for x in s.split() if not "@" in x and not "RT" in x])
    # print(splitted_line.most_common(5))


