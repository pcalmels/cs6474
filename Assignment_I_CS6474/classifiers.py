import nltk
import csv
from sklearn.model_selection import KFold
from sklearn.naive_bayes import MultinomialNB
import numpy as np
from collections import Counter


def load_csv(input_path):
    with open(input_path) as f:
        reader = csv.reader(f, delimiter=",")
        next(reader)
        data = []
        for row in reader:
            data.append([row[0], int(row[1])])
    f.close()
    return data


def k_cross_validation(dataset, k=5):
    kfold = KFold(n_splits=k, shuffle=True)
    X = dataset[:,0]
    Y = dataset[:,1]
    kfold.get_n_splits(X=X, y=Y)
    return kfold.split(X=X, y=Y)


# def define_labels(input_pos="results/PosSentiment_result.csv", input_neg="results/NegSentiment_result.csv"):
#     data_pos = load_csv(input_pos)
#     data_neg = load_csv(input_neg)
#     dict_data_pos = {k: v for k, v in data_pos}
#     dict_data_neg = {k: v for k, v in data_neg}
#     difference_data = [[x, np.sign(dict_data_pos.get(x, 0)-dict_data_neg.get(x, 0))] for x in set(dict_data_pos)|set(dict_data_neg)]
#     difference_data = np.array(difference_data)
#     return difference_data


def apply_three_classifiers(train_X, train_Y, test_X, test_Y):
    cl = MultinomialNB()
    cl.fit(train_X, train_Y)
    print('works')
    print(cl.predict(train_X))
    return cl



# if __name__=='__main__':
#     # data = define_labels()
#     splitted_sets = k_cross_validation(data)
#     for train_index, test_index in splitted_sets:
#         print("TRAIN shape {0} , TEST shape {1}".format(train_index.shape, test_index.shape))
#         print(data[train_index, 0].reshape(len(train_index),1))
#         print(data[train_index, 1].reshape(len(train_index),1))
#         apply_three_classifiers(
#             data[train_index, 0].reshape(len(train_index),1),
#             data[train_index, 1].reshape(len(train_index),1),
#             data[test_index, 0].reshape(len(test_index),1),
#             data[test_index, 1].reshape(len(test_index),1)
#         )
#         # print(data[train_index])